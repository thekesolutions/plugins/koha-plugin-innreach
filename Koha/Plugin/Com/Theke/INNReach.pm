package Koha::Plugin::Com::Theke::INNReach;

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This program comes with ABSOLUTELY NO WARRANTY;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

use Encode;
use List::MoreUtils qw(any);
use Module::Metadata;
use Mojo::JSON qw(decode_json encode_json);
use YAML::XS;

use C4::Context;
use C4::Circulation qw(AddIssue AddReturn);
use C4::Reserves qw(AddReserve);

use Koha::Biblios;
use Koha::Database;
use Koha::Items;
use Koha::Libraries;
use Koha::Patron::Categories;
use Koha::Patrons;

use Koha::Plugin::Com::Theke::INNReach::Contribution;
use Koha::Plugin::Com::Theke::INNReach::OAuth2;

BEGIN {
    my $path = Module::Metadata->find_module_by_name(__PACKAGE__);
    $path =~ s!\.pm$!/lib!;
    unshift @INC, $path;

    require INNReach::BackgroundJobs::BorrowingSite::ItemInTransit;
    require INNReach::BackgroundJobs::BorrowingSite::ItemReceived;
    require INNReach::BackgroundJobs::OwningSite::CancelRequest;
    require INNReach::BackgroundJobs::OwningSite::FinalCheckin;
    require INNReach::BackgroundJobs::OwningSite::ItemShipped;
}

our $VERSION = "5.3.21";

our $metadata = {
    name            => 'INN-Reach connector plugin for Koha',
    author          => 'Theke Solutions',
    date_authored   => '2018-09-10',
    date_updated    => "1980-06-18",
    minimum_version => '22.1100000',
    maximum_version => undef,
    version         => $VERSION,
    description     => 'INN-Reach ILL integration module.',
    namespace       => 'innreach',
};

=head1 Koha::Plugin::Com::Theke::INNReach

INN-Reach connector plugin for Koha

=head2 Plugin methods

=head3 new

Constructor:

    my $plugin = Koha::Plugin::Com::Theke::INNReach->new;

=cut

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    my $self = $class->SUPER::new($args);

    return $self;
}

=head3 configure

Plugin configuration method

=cut

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template( { file => 'configure.tt' } );

    if ( scalar $cgi->param('op') eq 'cud-save' ) {

        $self->store_data(
            {
                configuration => scalar $cgi->param('configuration'),
            }
        );
    }

    my $errors = $self->check_configuration;

    $template->param(
        errors        => $errors,
        configuration => $self->retrieve_data('configuration'),
    );

    $self->output_html( $template->output() );
}

=head3 configuration

Accessor for the de-serialized plugin configuration

=cut

sub configuration {
    my ($self) = @_;

    my $configuration;
    eval {
        $configuration = YAML::XS::Load(
            Encode::encode_utf8( $self->retrieve_data('configuration') ) );
    };
    die($@) if $@;

    my @default_item_types;

    foreach my $centralServer ( keys %{ $configuration } ) {
        # Reverse the library_to_location key
        my $library_to_location = $configuration->{$centralServer}->{library_to_location};
        $configuration->{$centralServer}->{location_to_library} =
          { map { $library_to_location->{$_}->{location} => $_ }
              keys %{$library_to_location} };

        # Reverse the local_to_central_patron_type key
        my $local_to_central_patron_type = $configuration->{$centralServer}->{local_to_central_patron_type};
        my %central_to_local_patron_type = reverse %{ $local_to_central_patron_type };
        $configuration->{$centralServer}->{central_to_local_patron_type} = \%central_to_local_patron_type;

        push @default_item_types, $configuration->{$centralServer}->{default_item_type}
            if exists $configuration->{$centralServer}->{default_item_type};

        $configuration->{$centralServer}->{debt_blocks_holds} //= 1;
        $configuration->{$centralServer}->{max_debt_blocks_holds} //= 100;
        $configuration->{$centralServer}->{expiration_blocks_holds} //= 1;
        $configuration->{$centralServer}->{restriction_blocks_holds} //= 1;
    }

    $configuration->{default_item_types} = \@default_item_types;

    return $configuration;
}

=head3 install

Install method. Takes care of table creation and initialization if required

=cut

sub install {
    my ( $self, $args ) = @_;

    my $dbh = C4::Context->dbh;

    my $task_queue = $self->get_qualified_table_name('task_queue');

    unless ( $self->_table_exists($task_queue) ) {
        $dbh->do(
            qq{
            CREATE TABLE $task_queue (
                `id`           INT(11) NOT NULL AUTO_INCREMENT,
                `object_type`  ENUM('biblio', 'item', 'circulation', 'holds') NOT NULL DEFAULT 'biblio',
                `object_id`    INT(11) NOT NULL DEFAULT 0,
                `payload`      TEXT DEFAULT NULL,
                `action`       ENUM('create','modify','delete','renewal','checkin','checkout','fill','cancel','b_item_in_transit','b_item_received','o_cancel_request','o_final_checkin','o_item_shipped') NOT NULL DEFAULT 'modify',
                `status`       ENUM('queued','retry','success','error','skipped') NOT NULL DEFAULT 'queued',
                `attempts`     INT(11) NOT NULL DEFAULT 0,
                `last_error`   VARCHAR(191) DEFAULT NULL,
                `timestamp`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `central_server` VARCHAR(10) NOT NULL,
                `run_after`    TIMESTAMP NULL DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `status` (`status`),
                KEY `central_server` (`central_server`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        }
        );
    }

    my $agency_to_patron = $self->get_qualified_table_name('agency_to_patron');

    unless ( $self->_table_exists($agency_to_patron) ) {
        $dbh->do(
            qq{
            CREATE TABLE $agency_to_patron (
                `central_server` VARCHAR(191) NOT NULL,
                `local_server`   VARCHAR(191) NULL DEFAULT NULL,
                `agency_id`      VARCHAR(191) NOT NULL,
                `patron_id`      INT(11) NOT NULL,
                `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`central_server`,`agency_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        }
        );
    }

    my $contributed_biblios = $self->get_qualified_table_name('contributed_biblios');

    unless ( $self->_table_exists($contributed_biblios) ) {
        $dbh->do(
            qq{
            CREATE TABLE $contributed_biblios (
                `central_server` VARCHAR(191) NOT NULL,
                `biblio_id`      INT(11) NOT NULL,
                `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`central_server`,`biblio_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        }
        );
    }

    my $contributed_items = $self->get_qualified_table_name('contributed_items');

    unless ( $self->_table_exists($contributed_items) ) {
        $dbh->do(
            qq{
            CREATE TABLE $contributed_items (
                `central_server` VARCHAR(191) NOT NULL,
                `item_id`        INT(11) NOT NULL,
                `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`central_server`,`item_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        }
        );
    }

    return 1;
}

=head3 upgrade

Takes care of upgrading whatever is needed (table structure, new tables, information on those)

=cut

sub upgrade {
    my ( $self, $args ) = @_;

    my $dbh = C4::Context->dbh;

    my $new_version = "1.1.0";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        unless ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                CREATE TABLE $task_queue (
                    `id`           INT(11) NOT NULL AUTO_INCREMENT,
                    `object_type`  ENUM('biblio', 'item') NOT NULL DEFAULT 'biblio',
                    `object_id`    INT(11) NOT NULL DEFAULT 0,
                    `action`       ENUM('create', 'modify', 'delete') NOT NULL DEFAULT 'modify',
                    `status`       ENUM('queued', 'retry', 'success', 'error') NOT NULL DEFAULT 'queued',
                    `attempts`     INT(11) NOT NULL DEFAULT 0,
                    `timestamp`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`),
                    KEY `status` (`status`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "1.1.18";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        unless ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    ADD COLUMN `last_error` VARCHAR(191) DEFAULT NULL AFTER `attempts`;
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "2.1.4";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $agency_to_patron =
          $self->get_qualified_table_name('agency_to_patron');

        unless ( $self->_table_exists($agency_to_patron) ) {
            $dbh->do(qq{
                CREATE TABLE $agency_to_patron (
                    `central_server` VARCHAR(191) NOT NULL,
                    `local_server`   VARCHAR(191) NULL DEFAULT NULL,
                    `agency_id`      VARCHAR(191) NOT NULL,
                    `patron_id`      INT(11) NOT NULL,
                    `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`central_server`,`agency_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "2.2.6";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    ADD COLUMN    `payload` TEXT DEFAULT NULL AFTER `object_id`,
                    MODIFY COLUMN `action`  ENUM('create', 'modify', 'delete', 'renew') NOT NULL DEFAULT 'modify';
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "2.3.0";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    ADD COLUMN `central_server` VARCHAR(10) NOT NULL AFTER `timestamp`;
            });
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    ADD KEY `central_server` (`central_server`);
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "2.6.12";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN `action` ENUM('create', 'modify', 'delete', 'renewal', 'checkin', 'checkout') NOT NULL DEFAULT 'modify';
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "3.3.14";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN `status` ENUM('queued', 'retry', 'success', 'error', 'skipped') NOT NULL DEFAULT 'queued';
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "3.4.0";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {
        my $contributed_biblios = $self->get_qualified_table_name('contributed_biblios');

        if ( !$self->_table_exists( $contributed_biblios ) ) {
            $dbh->do(qq{
                CREATE TABLE $contributed_biblios (
                    `central_server` VARCHAR(191) NOT NULL,
                    `biblio_id`      INT(11) NOT NULL,
                    `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`central_server`,`biblio_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            });
        }

        my $contributed_items = $self->get_qualified_table_name('contributed_items');

        if ( !$self->_table_exists( $contributed_items ) ) {
            $dbh->do(qq{
                CREATE TABLE $contributed_items (
                    `central_server` VARCHAR(191) NOT NULL,
                    `item_id`        INT(11) NOT NULL,
                    `timestamp`      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY (`central_server`,`item_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "3.8.1";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN `object_type` ENUM('biblio', 'item', 'circulation') NOT NULL DEFAULT 'biblio';
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "3.8.5";
    if (
        Koha::Plugins::Base::_version_compare(
            $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1
      )
    {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN `object_type` ENUM('biblio', 'item', 'circulation', 'holds') NOT NULL DEFAULT 'biblio';
            });

            $dbh->do(qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN `action` ENUM('create', 'modify', 'delete', 'renewal', 'checkin', 'checkout', 'fill', 'cancel') NOT NULL DEFAULT 'modify';
            });
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    $new_version = "5.3.16";
    if ( Koha::Plugins::Base::_version_compare( $self->retrieve_data('__INSTALLED_VERSION__'), $new_version ) == -1 ) {

        my $task_queue = $self->get_qualified_table_name('task_queue');

        if ( $self->_table_exists($task_queue) ) {
            $dbh->do(
                qq{
                ALTER TABLE $task_queue
                    MODIFY COLUMN
                    `action` ENUM(
                        'create',
                        'modify',
                        'delete',
                        'renewal',
                        'checkin',
                        'checkout',
                        'fill',
                        'cancel',
                        'b_item_in_transit',
                        'b_item_received',
                        'o_cancel_request',
                        'o_final_checkin',
                        'o_item_shipped')
                    NOT NULL DEFAULT 'modify';
            }
            );

            $dbh->do(
                qq{
                ALTER TABLE $task_queue
                    ADD COLUMN `run_after` TIMESTAMP NULL DEFAULT NULL AFTER `central_server`;
            }
            );
        }

        $self->store_data( { '__INSTALLED_VERSION__' => $new_version } );
    }

    return 1;
}

=head3 _table_exists (helper)

Method to check if a table exists in Koha.

FIXME: Should be made available to plugins in core

=cut

sub _table_exists {
    my ($self, $table) = @_;
    eval {
        C4::Context->dbh->{PrintError} = 0;
        C4::Context->dbh->{RaiseError} = 1;
        C4::Context->dbh->do(qq{SELECT * FROM $table WHERE 1 = 0 });
    };
    return 1 unless $@;
    return 0;
}

=head3 after_biblio_action

Hook that is called on biblio modification

=cut

sub after_biblio_action {
    my ( $self, $args ) = @_;

    my $action    = $args->{action};
    my $biblio_id = $args->{biblio_id};
    my $biblio    = $args->{biblio};

    my $configuration   = $self->configuration;
    my @central_servers = $self->central_servers;

    foreach my $central_server ( @central_servers ) {

        # skip if contribution disabled
        next
            unless $configuration->{$central_server}->{contribution}->{enabled};

        my $contribution = $self->contribution($central_server);

        if ( $action eq 'create' || $action eq 'modify' ) {
            if ( $contribution->should_biblio_be_contributed( { biblio => $biblio } )
                )
            {
                $self->schedule_task(
                    {
                        action         => $action,
                        central_server => $central_server,
                        object_id      => $biblio_id,
                        object_type    => 'biblio',
                        status         => 'queued',
                    }
                );
            } elsif (
                $contribution->is_bib_contributed( { biblio_id => $biblio_id } ) )
            {
                # decontribute
                $self->schedule_task(
                    {
                        action         => 'delete',
                        central_server => $central_server,
                        object_id      => $biblio_id,
                        object_type    => 'biblio',
                        status         => 'queued',
                    }
                );
            }
        } elsif ( $action eq 'delete' ) {
            if ( $contribution->is_bib_contributed( { biblio_id => $biblio_id } ) ) {
                $self->schedule_task(
                    {
                        action         => 'delete',
                        central_server => $central_server,
                        object_id      => $biblio_id,
                        object_type    => 'biblio',
                        status         => 'queued',
                    }
                );
            }
        }
    }

    return;
}

=head3 after_item_action

Hook that is caled on item modification

=cut

sub after_item_action {
    my ( $self, $args ) = @_;

    my $action  = $args->{action};
    my $item_id = $args->{item_id};
    my $item    = $args->{item};

    my $configuration   = $self->configuration;
    my @central_servers = $self->central_servers;

    foreach my $central_server ( @central_servers ) {

        # skip if contribution disabled
        next
            unless $configuration->{$central_server}->{contribution}->{enabled};

        my $contribution = $self->contribution($central_server);

        if ( $action ne 'delete' ) {

            my $item_type = $item->itype;

            # We don't contribute ILL-generated items
            next
                if $item_type eq $configuration->{$central_server}->{default_item_type};

            # Skip if rules say so
            next
                if ( !$contribution->should_item_be_contributed( { item => $item } ) );

            # Skip if item type is not mapped
            if ( !exists $configuration->{$central_server}->{local_to_central_itype}->{$item_type} ) {
                $self->innreach_warn("No 'local_to_central_itype' mapping for $item_type ($central_server)");
                next;
            }
        }

        $self->schedule_task(
            {
                action         => $action,
                central_server => $central_server,
                object_id      => $item_id,
                object_type    => 'item',
                status         => 'queued',
            }
        );

        # Do it after inserting the item de-contribution
        if ( $action eq 'delete' ) {

            my $exclude_empty_biblios =
              ( !exists $configuration->{$central_server}->{contribution} )
              ? 1
              : $configuration->{$central_server}->{contribution}->{exclude_empty_biblios};

            if ( $exclude_empty_biblios ) {

                my $biblio = Koha::Biblios->find( $item->biblionumber );

                if (    $biblio
                    and $contribution->filter_items_by_contributable( { items => $biblio->items } )->count == 0 )
                {
                    $self->schedule_task(
                        {
                            action         => 'delete',
                            central_server => $central_server,
                            object_id      => $biblio->id,
                            object_type    => 'biblio',
                            status         => 'queued',
                        }
                    );
                }
            }
        }
    }

    return;
}

=head3 after_circ_action

Hook that is caled on circulation actions

Note: Koha updates item statuses on circulation, so circulation updates on items
are notified by the I<after_item_action> hook.

So far, only renewals of ILL-linked items are taken care of here.

=cut

sub after_circ_action {
    my ( $self, $params ) = @_;

    my $action   = $params->{action};
    my $checkout = $params->{payload}->{checkout};

    my $req;

    if ( $action eq 'checkout' )
    {    # we don't have a checkout_id yet. the item has been created by itemshipped so query using the barcode
         # this only applies to the borrowing side. On the lending side all the workflow is handled
         # within the hold cancel/fill actions, which trigger plain cancellation or setting the status as
         # O_ITEM_SHIPPED and generating the checkout right after, inside the same transaction.
        $req = $self->get_ill_requests_from_attribute(
            {
                type  => 'itemBarcode',
                value => $checkout->item->barcode,
            }
        )->search(
            { 'me.status' => [ 'B_ITEM_RECEIVED', 'B_ITEM_RECALLED' ] },
            { order_by    => { '-desc' => 'updated' }, rows => 1, }
        )->single;
    } else {
        $req = $self->get_ill_request_from_attribute(
            {
                type  => 'checkout_id',
                value => $checkout->id,
            }
        );
    }

    return
        unless $req;

    my $configuration  = $self->configuration;
    my $central_server = $self->get_req_central_server($req);

    if ( $action eq 'renewal' ) {
        $params->{payload}->{date_due} = $params->{payload}->{checkout}->date_due;
        $self->schedule_task(
            {
                action         => $action,
                central_server => $central_server,
                object_id      => $checkout->item->id,
                object_type    => 'item',
                payload        => encode_json( $params->{payload} ),
                status         => 'queued',
            }
        );
    } elsif ( $action eq 'checkin' ) {

        if (
            any { $req->status eq $_ }
            qw(O_ITEM_CANCELLED
            O_ITEM_CANCELLED_BY_US
            O_ITEM_CLAIMED_RETURNED
            O_ITEM_IN_TRANSIT
            O_ITEM_RETURN_UNCIRCULATED
            O_ITEM_RECEIVED_DESTINATION)
            )
        {
            INNReach::BackgroundJobs::OwningSite::FinalCheckin->new->enqueue(
                {
                    ill_request_id => $req->id,
                }
            ) if $configuration->{$central_server}->{lending}->{automatic_final_checkin};
        } elsif ( any { $req->status eq $_ } qw{B_ITEM_RECEIVED B_ITEM_RECALLED} ) {
            INNReach::BackgroundJobs::BorrowingSite::ItemInTransit->new->enqueue(
                {
                    ill_request_id => $req->id,
                }
            ) if $configuration->{$central_server}->{borrowing}->{automatic_item_in_transit};
        }
    } elsif ( $action eq 'checkout' ) {
        if ( any { $req->status eq $_ } qw{B_ITEM_RECEIVED} ) {
            $self->add_or_update_attributes(
                {
                    request    => $req,
                    attributes => { checkout_id => $checkout->id }
                }
            );
        }
    }

    return;
}

=head3 after_hold_action

Hook that is caled on holds-related actions

=cut

sub after_hold_action {
    my ( $self, $params ) = @_;

    my $action = $params->{action};

    my $payload = $params->{payload};
    my $hold    = $payload->{hold};

    my $req = $self->get_ill_request_from_attribute(
        {
            type  => 'hold_id',
            value => $hold->id,
        }
    );

    # skip actions if this is not an ILL request
    return
        unless $req;

    my $configuration  = $self->configuration;
    my $central_server = $self->get_req_central_server($req);

    if ( $req->status =~ /^O_/ ) {
        if ( $action eq 'fill' || $action eq 'waiting' || $action eq 'transfer' ) {

            if ( $req->status eq 'O_ITEM_REQUESTED' ) {

                INNReach::BackgroundJobs::OwningSite::ItemShipped->new->enqueue(
                    {
                        ill_request_id => $req->id,
                    }
                ) if $configuration->{$central_server}->{lending}->{automatic_item_shipped};
            }
        } elsif ( $action eq 'cancel' ) {

            if ( $req->status eq 'O_ITEM_REQUESTED' ) {

                INNReach::BackgroundJobs::OwningSite::CancelRequest->new->enqueue(
                    {
                        ill_request_id => $req->id,
                    }
                );
            }
        }
    }
    elsif ( $req->status =~ /^B_/ ) {
        if ( $action eq 'fill' || $action eq 'waiting' || $action eq 'transfer' ) {
            if ( $req->status eq 'B_ITEM_SHIPPED' ) {

                INNReach::BackgroundJobs::BorrowingSite::ItemReceived->new->enqueue(
                    {
                        ill_request_id => $req->id,
                    }
                ) if $configuration->{$central_server}->{borrowing}->{automatic_item_receive};
            }
        }
    }

    return;
}

=head3 schedule_task

    $plugin->schedule_task(
        {
            action         => $action,
            central_server => $central_server,
            object_type    => $object_type,
            object_id      => $object->id
        }
    );

Method for adding tasks to the queue

=cut

sub schedule_task {
    my ( $self, $params ) = @_;

    my @mandatory_params = qw(action central_server object_type object_id);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $params->{$param};
    }

    my $action         = $params->{action};
    my $central_server = $params->{central_server};
    my $object_type    = $params->{object_type};
    my $object_id      = $params->{object_id};
    my $payload        = $params->{payload};

    $payload = ""
        unless $payload;

    my $task_queue = $self->get_qualified_table_name('task_queue');

    C4::Context->dbh->do(
        qq{
        INSERT INTO $task_queue
            (  central_server,     object_type,   object_id,   action,   status,  attempts,  payload )
        VALUES
            ( '$central_server', '$object_type', $object_id, '$action', 'queued',        0, '$payload' );
    }
    );

    return $self;
}

=head3 api_routes

Method that returns the API routes to be merged into Koha's

=cut

sub api_routes {
    my ( $self, $args ) = @_;

    my $spec_str = $self->mbf_read('openapi.json');
    my $spec     = decode_json($spec_str);

    return $spec;
}

=head3 api_namespace

Method that returns the namespace for the plugin API to be put on

=cut

sub api_namespace {
    my ( $self ) = @_;
    
    return 'innreach';
}

=head3 background_tasks

Plugin hook used to register new background_job types

=cut

sub background_tasks {
    return {
        b_item_in_transit => 'INNReach::BackgroundJobs::BorrowingSite::ItemInTransit',
        b_item_received   => 'INNReach::BackgroundJobs::BorrowingSite::ItemReceived',
        o_cancel_request  => 'INNReach::BackgroundJobs::OwningSite::CancelRequest',
        o_final_checkin   => 'INNReach::BackgroundJobs::OwningSite::FinalCheckin',
        o_item_shipped    => 'INNReach::BackgroundJobs::OwningSite::ItemShipped',
    };
}

=head3 template_include_paths

Plugin hook used to register paths to find templates

=cut

sub template_include_paths {
    my ($self) = @_;

    return [
        $self->mbf_path('templates'),
    ];
}

=head3 cronjob_nightly

Plugin hook for running nightly tasks

=cut

sub cronjob_nightly {
    my ($self) = @_;

    foreach my $central_server ( $self->central_servers ) {
        $self->sync_agencies($central_server);
    }
}

=head2 Business methods

=head3 generate_patron_for_agency

    my $patron = $plugin->generate_patron_for_agency(
        {
            central_server => $central_server,
            local_server   => $local_server,
            agency_id      => $agency_id,
            description    => $description
        }
    );

Generates a patron representing a library in the consortia that might make
material requests (borrowing site). It is used on the circulation workflow to
place a hold on the requested items.

=cut

sub generate_patron_for_agency {
    my ( $self, $args ) = @_;

    my @mandatory_params = qw(central_server local_server description agency_id);
    foreach my $param ( @mandatory_params ) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $args->{$param};
    }

    my $central_server = $args->{central_server};
    my $local_server   = $args->{local_server};
    my $description    = $args->{description};
    my $agency_id      = $args->{agency_id};

    my $agency_to_patron = $self->get_qualified_table_name('agency_to_patron');

    my $library_id    = $self->configuration->{$central_server}->{partners_library_id};
    my $category_code = $self->configuration->{$central_server}->{partners_category}
        // C4::Context->config("interlibrary_loans")->{partner_code};

    my $patron;

    Koha::Database->schema->txn_do( sub {
        $patron = Koha::Patron->new(
            {
                branchcode   => $library_id,
                categorycode => $category_code,
                surname      => $self->gen_patron_description(
                    {
                        central_server => $central_server,
                        local_server   => $local_server,
                        description    => $description,
                        agency_id      => $agency_id
                    }
                ),
                cardnumber => $self->gen_cardnumber(
                    {
                        central_server => $central_server,
                        local_server   => $local_server,
                        description    => $description,
                        agency_id      => $agency_id
                    }
                )
            }
        )->store;

        my $patron_id = $patron->borrowernumber;

        my $dbh = C4::Context->dbh;
        my $sth = $dbh->prepare(qq{
            INSERT INTO $agency_to_patron
              ( central_server, local_server, agency_id, patron_id )
            VALUES
              ( '$central_server', '$local_server', '$agency_id', '$patron_id' );
        });

        $sth->execute();
    });

    return $patron;
}

=head3 update_patron_for_agency

    my $patron = $plugin->update_patron_for_agency(
        {
            central_server => $central_server,
            local_server   => $local_server,
            agency_id      => $agency_id,
            description    => $description
        }
    );

Updates a patron representing a library in the consortia that might make
material requests (borrowing site). It is used by cronjobs to keep things up
to date if there are changes on the central server info.

See: scripts/sync_agencies.pl

=cut

sub update_patron_for_agency {
    my ( $self, $args ) = @_;

    my $central_server = $args->{central_server};
    my $local_server   = $args->{local_server};
    my $description    = $args->{description};
    my $agency_id      = $args->{agency_id};

    my $agency_to_patron = $self->get_qualified_table_name('agency_to_patron');

    my $library_id    = $self->configuration->{$central_server}->{partners_library_id};
    my $category_code = C4::Context->config("interlibrary_loans")->{partner_code};

    my $patron;

    Koha::Database->schema->txn_do( sub {

        my $patron_id = $self->get_patron_id_from_agency({
            central_server => $central_server,
            agency_id      => $agency_id
        });

        $patron = Koha::Patrons->find( $patron_id );
        $patron->set(
            {
                surname => $self->gen_patron_description(
                    {
                        central_server => $central_server,
                        local_server   => $local_server,
                        description    => $description,
                        agency_id      => $agency_id
                    }
                ),
                cardnumber => $self->gen_cardnumber(
                    {
                        central_server => $central_server,
                        local_server   => $local_server,
                        description    => $description,
                        agency_id      => $agency_id
                    }
                )
            }
        )->store;
    });

    return $patron;
}

=head3 get_patron_id_from_agency

    my $patron_id = $plugin->get_patron_id_from_agency(
        {
            central_server => $central_server,
            agency_id      => $agency_id
        }
    );

Given an agency_id (which usually comes in the patronAgencyCode attribute on the itemhold request)
and a central_server code, it returns Koha's patron id so the hold request can be correctly assigned.

=cut

sub get_patron_id_from_agency {
    my ( $self, $args ) = @_;

    my $central_server = $args->{central_server};
    my $agency_id      = $args->{agency_id};

    my $agency_to_patron = $self->get_qualified_table_name('agency_to_patron');
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare(qq{
        SELECT patron_id
        FROM   $agency_to_patron
        WHERE  agency_id='$agency_id' AND central_server='$central_server'
    });

    $sth->execute();
    my $result = $sth->fetchrow_hashref;

    unless ($result) {
        return;
    }

    return $result->{patron_id};
}

=head3 gen_patron_description

    my $patron_description = $plugin->gen_patron_description(
        {
            central_server => $central_server,
            local_server   => $local_server,
            description    => $description,
            agency_id      => $agency_id
        }
    );

This method encapsulates patron description generation based on the provided information.
The idea is that any change on this regard should happen on a single place.

=cut

sub gen_patron_description {
    my ( $self, $args ) = @_;

    my $central_server = $args->{central_server};
    my $local_server   = $args->{local_server};
    my $description    = $args->{description};
    my $agency_id      = $args->{agency_id};

    return "$description ($agency_id)";
}

=head3 gen_cardnumber

    my $cardnumber = $plugin->gen_cardnumber(
        {
            central_server => $central_server,
            local_server   => $local_server,
            description    => $description,
            agency_id      => $agency_id
        }
    );

This method encapsulates patron description generation based on the provided information.
The idea is that any change on this regard should happen on a single place.

=cut

sub gen_cardnumber {
    my ( $self, $args ) = @_;

    my $central_server = $args->{central_server};
    my $local_server   = $args->{local_server};
    my $description    = $args->{description};
    my $agency_id      = $args->{agency_id};

    return 'ILL_' . $central_server . '_' . $agency_id;
}

=head3 central_servers

    my @central_servers = $self->central_servers;

=cut

sub central_servers {
    my ( $self ) = @_;

    my $configuration = $self->configuration;

    if ( defined $configuration ) {
        return grep { $_ ne 'default_item_types' } keys %{ $configuration };
    }

    return ();
}

=head3 get_ill_request_from_biblio_id

This method retrieves the ILL request using a biblio_id.

=cut

sub get_ill_request_from_biblio_id {
    my ( $self, $args ) = @_;

    my $biblio_id = $args->{biblio_id};

    unless ( $biblio_id ) {
        INNReach::Ill::UnknownBiblioId->throw( biblio_id => $biblio_id );
    }

    my $reqs = $self->get_ill_rs()->search({ biblio_id => $biblio_id });

    if ( $reqs->count > 1 ) {
        $self->innreach_warn( "More than one ILL request for biblio_id ($biblio_id). Beware!");
    }

    return unless $reqs->count > 0;

    my $req = $reqs->next;

    return $req;
}

=head3 get_ill_request

This method retrieves the ILL request using the I<trackingId> and
I<centralCode> attributes.

=cut

sub get_ill_request {
    my ($self, $args) = @_;

    my $trackingId  = $args->{trackingId};
    my $centralCode = $args->{centralCode};

    # Get/validate the request
    my $dbh = C4::Context->dbh;
    my $sth = $dbh->prepare(
        qq{
        SELECT * FROM illrequestattributes AS ra_a
        INNER JOIN    illrequestattributes AS ra_b
        ON ra_a.illrequest_id=ra_b.illrequest_id AND
          (ra_a.type='trackingId'  AND ra_a.value='$trackingId') AND
          (ra_b.type='centralCode' AND ra_b.value='$centralCode');
    }
    );

    $sth->execute();
    my $result = $sth->fetchrow_hashref;

    my $req;

    $req = $self->get_ill_rs->find( $result->{illrequest_id} )
        if $result->{illrequest_id};

    return $req;
}


=head3 get_ua

This method retrieves a user agent to contact a central server.

=cut

sub get_ua {
    my ( $self, $central_server ) = @_;

    my $configuration = $self->configuration->{$central_server};

    unless ( $self->{_oauth2}->{$central_server} ) {
        $self->{_oauth2}->{$central_server} = Koha::Plugin::Com::Theke::INNReach::OAuth2->new({
            client_id          => $configuration->{client_id},
            client_secret      => $configuration->{client_secret},
            api_base_url       => $configuration->{api_base_url},
            api_token_base_url => $configuration->{api_token_base_url},
            local_server_code  => $configuration->{localServerCode},
            debug_requests     => $configuration->{debug_requests},
        });
    }

    return $self->{_oauth2}->{$central_server};
}

=head3 debug_mode

    if ( $self->debug_mode($central_server) ) { ... }

This method tells if debug mode is enabled/configured for the specified I<$central_server>.

Defaults to B<0> if not specified in the configuration YAML.

=cut

sub debug_mode {
    my ( $self, $central_server ) = @_;

    return $self->configuration->{$central_server}->{debug_mode} ? 1 : 0;
}

=head3 contribution

    my $contribution = $plugin->contribution($central_server);

This method retrieves a I<Koha::Plugin::Com::Theke::INNReach::Contribution> object for the passed
I<$central_server>

=cut

sub contribution {
    my ( $self, $central_server ) = @_;

    unless ( $self->{_contribution}->{$central_server} ) {
        $self->{_contribution}->{$central_server} = Koha::Plugin::Com::Theke::INNReach::Contribution->new(
            { central_server => $central_server, plugin => $self } );
    }

    return $self->{_contribution}->{$central_server};
}

=head3 get_req_central_server

This method returns the central server code a Koha::Illrequest is linked to.

=cut

sub get_req_central_server {
    my ( $self, $req ) = @_;

    my $attr = $req->extended_attributes->find( { type => 'centralCode' } );

    return $attr->value
        if $attr;

    return;
}


=head2 Wrappers for Koha functions

This methods deal with changes to the ABI of the underlying methods
across different Koha versions.

=head3 add_issue

    my $checkout = $plugin->add_issue(
        {
            patron  => $patron,
            barcode => $barcode
        }
    );

Wrapper for I<C4::Circulation::AddIssue>.

Parameters:

=over

=item B<patron>: a I<Koha::Patron> object.

=item B<barcode>: a I<string> containing an item barcode.

=back

=cut

sub add_issue {
    my ( $self, $params ) = @_;

    my @mandatory_params = qw(barcode patron);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $params->{$param};
    }

    return ( C4::Context->preference('Version') ge '23.110000' )
        ? AddIssue( $params->{patron},            $params->{barcode} )
        : AddIssue( $params->{patron}->unblessed, $params->{barcode} );
}

=head3 add_return

    $plugin->add_return( { barcode => $barcode } );

Wrapper for I<C4::Circulation::AddReturn>. The return value is the
same as C4::Circulation::AddReturn.

Parameters:

=over

=item B<barcode>: a I<string> containing an item barcode.

=back

=cut

sub add_return {
    my ( $self, $params ) = @_;

    my @mandatory_params = qw(barcode);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $params->{$param};
    }

    return AddReturn( $params->{barcode} );
}

=head3 add_hold

    my $hold_id = $plugin->add_hold(
        {
            library_id => $library_id,
            patron_id  => $patron_id,
            biblio_id  => $biblio_id,
            notes      => $notes,
            item_id    => $item_id,
        }
    );

Wrapper for I<C4::Reserves::AddReserve>.

Parameters: all AddReserve parameters

=cut

sub add_hold {
    my ( $self, $params ) = @_;

    my @mandatory_params = qw(library_id patron_id biblio_id item_id);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $params->{$param};
    }

    return AddReserve(
        {
            branchcode       => $params->{library_id},
            borrowernumber   => $params->{patron_id},
            biblionumber     => $params->{biblio_id},
            priority         => 1,
            reservation_date => undef,
            expiration_date  => undef,
            notes            => $params->{notes} // 'Placed by ILL',
            title            => '',
            itemnumber       => $params->{item_id},
            found            => undef,
            itemtype         => undef
        }
    );
}

=head3 get_ill_rs

    my $ill_rs = $self->get_ill_rs;

This method wraps the ILL resultset class to make it future proof.

=cut

sub get_ill_rs {
    my ( $self ) = @_;

    my $rs;

    if ( C4::Context->preference('Version') ge '24.050000' ) {
        require Koha::ILL::Requests;
        $rs = Koha::ILL::Requests->new;
    } else {
        require Koha::Illrequests;
        $rs = Koha::Illrequests->new;
    }

    return $rs;
}

=head3 new_ill_request

    my $req = $self->new_ill_request( $params );

This method wraps the method to create a new ILL request
to make it future proof.

=cut

sub new_ill_request {
    my ( $self, $params ) = @_;

    my $req;

    if ( C4::Context->preference('Version') ge '24.050000' ) {
        require Koha::ILL::Request;
        $req = Koha::ILL::Request->new($params);
    } else {
        require Koha::Illrequest;
        $req = Koha::Illrequest->new($params);
    }

    return $req;
}

=head3 new_ill_request_attr

    my $req = $self->new_ill_request_attr( $params );

This method wraps the method to create a new ILL request attribute
to make it future proof.

=cut

sub new_ill_request_attr {
    my ( $self, $params ) = @_;

    my $attr;

    if ( C4::Context->preference('Version') ge '24.050000' ) {
        require Koha::ILL::Request::Attribute;
        $attr = Koha::ILL::Request::Attribute->new($params);
    } else {
        require Koha::Illrequestattribute;
        $attr = Koha::Illrequestattribute->new($params);
    }

    return $attr;
}

=head3 check_configuration

    my $errors = $self->check_configuration;

Returns a reference to a list of errors found in configuration.

=cut

sub check_configuration {
    my ($self) = @_;

    my @errors;

    my $configuration   = $self->configuration;
    my @central_servers = grep { $_ ne 'default_item_types' } keys %{$configuration};

    foreach my $central_server (@central_servers) {
        push @errors,
            {
            code  => 'undefined_partners_library_id',
            value => $configuration->{$central_server}->{partners_library_id}, central_server => $central_server
            }
            unless Koha::Libraries->find( $configuration->{$central_server}->{partners_library_id} );
    }

    foreach my $central_server (@central_servers) {
        push @errors,
            {
            code  => 'undefined_partners_category',
            value => $configuration->{$central_server}->{partners_category}, central_server => $central_server
            }
            unless Koha::Patron::Categories->find( $configuration->{$central_server}->{partners_category} );
    }

    foreach my $central_server (@central_servers) {
        my $library_to_location = $configuration->{$central_server}->{library_to_location};
        foreach my $key ( keys %{$library_to_location} ) {

            # do we have a location code?
            push @errors,
                {
                code           => 'library_missing_location',
                central_server => $central_server,
                library        => $key,
                }
                unless $library_to_location->{$key}->{location};

            # do we have a description?
            push @errors,
                {
                code           => 'library_missing_description',
                central_server => $central_server,
                library        => $key,
                }
                unless $library_to_location->{$key}->{description};
        }
    }

    push @errors, { code => 'ILLModule_disabled' }
        unless C4::Context->preference('ILLModule');

    push @errors, { code => 'CirculateILL_enabled' }
        if C4::Context->preference('CirculateILL');

    return \@errors;
}

=head3 sync_agencies

    my $result = $self->sync_agencies;

Syncs server agencies with the current patron's database. Returns a hashref
with the following structure:

    {
        localCode_1 => {
            agencyCode_1 => {
                description    => "The agency name",
                current_status => no_entry|entry_exists|invalid_entry,
                status         => created|updated
            },
            ...
        },
        ...
    }

=cut

sub sync_agencies {
    my ($self, $central_server) = @_;

    my $response = $self->contribution($central_server)->get_agencies_list();

    my $result = {};

    foreach my $server (@{$response}) {
        my $local_server = $server->{localCode};
        my $agency_list  = $server->{agencyList};

        foreach my $agency ( @{$agency_list} ) {

            my $agency_id   = $agency->{agencyCode};
            my $description = $agency->{description};

            $result->{$local_server}->{$agency_id}->{description} = $description;

            my $patron_id = $self->get_patron_id_from_agency(
                {
                    central_server => $central_server,
                    agency_id      => $agency_id,
                }
            );

            my $patron;

            $result->{$local_server}->{$agency_id}->{current_status} = 'no_entry';

            if ( $patron_id ) {
                $result->{$local_server}->{$agency_id}->{current_status} = 'entry_exists';
                $patron = Koha::Patrons->find( $patron_id );
            }

            if ($patron_id && !$patron) {

                # cleanup needed!
                $self->innreach_warn( "There is a 'agency_to_patron' entry for '$agency_id', but the patron is not present on the DB!");
                my $agency_to_patron = $self->get_qualified_table_name('agency_to_patron');

                my $sth = C4::Context->dbh->prepare(qq{
                    DELETE FROM $agency_to_patron
                    WHERE patron_id='$patron_id';
                });

                $sth->execute();
                $result->{$local_server}->{$agency_id}->{current_status} = 'invalid_entry';
            }

            if ($patron) {

                # Update description
                $self->update_patron_for_agency(
                    {
                        agency_id      => $agency_id,
                        description    => $description,
                        local_server   => $local_server,
                        central_server => $central_server
                    }
                );
                $result->{$local_server}->{$agency_id}->{status} = 'updated';
            } else {

                # Create it
                $self->generate_patron_for_agency(
                    {
                        agency_id      => $agency_id,
                        description    => $description,
                        local_server   => $local_server,
                        central_server => $central_server
                    }
                );
                $result->{$local_server}->{$agency_id}->{status} = 'created';
            }
        }
    }

    return $result;
}

=head3 get_ill_request_from_attribute

    my $req = $plugin->get_ill_request_from_attribute(
        {
            type  => $type,
            value => $value
        }
    );

Retrieve an ILL request using some attribute.

=cut

sub get_ill_request_from_attribute {
    my ( $self, $args ) = @_;

    my @mandatory_params = qw(type value);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $args->{$param};
    }

    my $type  = $args->{type};
    my $value = $args->{value};

    my $requests_rs = $self->get_ill_rs()->search(
        {
            'illrequestattributes.type'  => $type,
            'illrequestattributes.value' => $value,
            'me.backend'                 => 'INNReach',
        },
        { join => ['illrequestattributes'] }
    );

    my $count = $requests_rs->count;

    $self->innreach_warn("more than one result searching requests with type='$type' value='$value'")
        if $count > 1;

    return $requests_rs->next
        if $count > 0;
}

=head3 get_ill_requests_from_attribute

    my $reqs = $plugin->get_ill_requests_from_attribute(
        {
            type  => $type,
            value => $value
        }
    );

Retrieve all ILL requests for the I<INNReach> backend with extended attributes
matching the passed parameters.

=cut

sub get_ill_requests_from_attribute {
    my ( $self, $args ) = @_;

    my @mandatory_params = qw(type value);
    foreach my $param (@mandatory_params) {
        INNReach::Ill::MissingParameter->throw( param => $param )
            unless exists $args->{$param};
    }

    my $type  = $args->{type};
    my $value = $args->{value};

    return $self->get_ill_rs()->search(
        {
            'illrequestattributes.type'  => $type,
            'illrequestattributes.value' => $value,
            'me.backend'                 => 'INNReach',
        },
        { join => ['illrequestattributes'] }
    );
}

=head3 add_or_update_attributes

    $plugin->add_or_update_attributes(
        {
            request    => $request,
            attributes => {
                $type_1 => $value_1,
                $type_2 => $value_2,
                ...
            },
        }
    );

Takes care of updating or adding attributes if they don't already exist.

=cut

sub add_or_update_attributes {
    my ( $self, $params ) = @_;

    my $request    = $params->{request};
    my $attributes = $params->{attributes};

    Koha::Database->new->schema->txn_do(
        sub {
            while ( my ( $type, $value ) = each %{$attributes} ) {

                my $attr = $request->extended_attributes->find( { type => $type } );

                if ($attr) {    # update
                    warn "ERROR: Attempt to set 'undef' for attribute of type '$type'"
                        unless defined $value;

                    if ( $attr->value ne $value ) {
                        $attr->update( { value => $value, } );
                    }
                } else {        # new
                    $attr = $self->new_ill_request_attr(
                        {
                            illrequest_id => $request->id,
                            type          => $type,
                            value         => $value,
                        }
                    )->store;
                }
            }
        }
    );

    return;
}

=head3 innreach_warn

Helper method for logging warnings for the INN-Reach plugin homogeneously.

=cut

sub innreach_warn {
    my ( $self, $warning ) = @_;

    warn "innreach_plugin_warn: $warning";
}

1;
